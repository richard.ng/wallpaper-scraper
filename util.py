import os
import random

# get a random list of files in path
def get_random_files(path):
  files = []
  for root, dirs, file in os.walk(path):
    for name in file:
        files.append(os.path.join(root, name))
  rand = random.sample(files,len(files))
  return files, rand

# remove files in path
def remove_files(path):
  for root, dirs, file in os.walk(path):
    for name in file:
      os.remove(os.path.join(root, name))