from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
import datetime
import os
import threading

import random
import time
from sys import platform
import util
import random_wall
# import spiders to use
from spiders.redditspider import RedditSpider
from spiders.unsplashspider import UnsplashSpider

# image path
imgpath = '{}/images/'.format(os.path.dirname(os.path.abspath(__file__)))

now = datetime.datetime.now().strftime("%m-%d")

def change():
  global files
  global rand
  global flag
  flag = 0
  # wait for scraping to finish, sets flag = 1
  while flag == 0:
    time.sleep(5)
  while True:
    # filename is first file in list
    filename = rand[0]
    # set wallpaper
    if platform == "linux" or platform == "linux2":
      os.system("/usr/bin/gsettings set org.gnome.desktop.background picture-uri '{}'".format(filename))
    elif platform == "darwin":
      scpt = """osascript -e 'tell application "System Events" to tell every desktop to set picture to "{0}"'""".format(filename)
      os.system(scpt)
    # print time of last scrape
    print("Wallpaper last changed at {}".format(datetime.datetime.now().strftime("%Y-%m-%d %I:%M:%S %p")))
    # remove file in list
    rand.pop(0)
    # wait X seconds, 1800 sec = 30 min
    time.sleep(1800)
    # if length of list is <=1, reset with a new random list
    if len(rand) <= 1:
      _, rand = util.get_random_files(imgpath)

def schedule_next_crawl(null, sleep_time):
  print("Wallpaper last scraped at {}".format(datetime.datetime.now().strftime("%Y-%m-%d %I:%M:%S %p")))
  global files
  global rand
  global flag
  files, rand = util.get_random_files(imgpath)
  flag = 1
  reactor.callLater(sleep_time, crawl)

@defer.inlineCallbacks
def crawl():
  util.remove_files(imgpath)
  
  runner = CrawlerRunner()

  # add additional spiders here
  if now > "11-15" or now < "01-15":
    # Christmas!! :D
    print("Merry Christmas!! :D")
    print("Getting random christmas wallpapers...")
    random_wall.random_christmas(imgpath)
  else:
    print("Running RedditSpider...")
    yield runner.crawl(RedditSpider)
    print("Running UnsplashSpider...")
    yield runner.crawl(UnsplashSpider)

  d = runner.join()

  # call schedule_next_crawl(<scrapy response>, n) after crawl job is complete
  d.addCallback(schedule_next_crawl, 36000) # 36000 sec = 10 hours

threading.Thread(target=change,daemon=True).start()
crawl()
reactor.run()