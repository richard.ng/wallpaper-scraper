# Wallpaper scraper

Currently this scraper will download wallpapers from:

- https://www.reddit.com/r/wallpapers/top
- https://unsplash.com/wallpapers
- https://unsplash.com/wallpapers/nature
- https://unsplash.com/wallpapers/desktop

The changer script will create a randomly ordered list of the pulled image files and change wallpapers at the desired interval. For each wallpaper change, the image file is removed from the list to avoid a repeated wallpaper. Once the list is empty, another randomly ordered list is created and the loop will continue.

## Usage

This wallpaper scraper only works for Linux and Mac.

*tmux is recommended*

Run `python3 walls.py` to pull images automatically every ten hours and change the wallpaper automatically every 30 minutes. This will continue to run in the background until terminated.

To customize the length of time between image scrapes and changes, modify the following: `d.addCallback(schedule_next_crawl, 36000)` and `time.sleep(1800)` (both in seconds), repectively.

You can then detach from the tmux session and leave the script running in the background.