import requests
import re
import os

def random_christmas(path):
    os.chdir(path)

    for i in range(1,10):
        r = requests.get("https://source.unsplash.com/random/5120x3200/?christmas", allow_redirects=True)

        img = requests.get(r.url).content

        imgname = re.search(r'photo[^\?]*', r.url).group() + ".jpg"

        with open(f'{imgname}', 'wb') as file:
            file.write(img)