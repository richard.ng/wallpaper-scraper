import scrapy
import datetime
# from scrapy.crawler import CrawlerProcess

now = datetime.datetime.now().strftime("%m-%d")

class UnsplashSpider(scrapy.Spider):
  name = 'UnsplashSpider'
  custom_settings = {
    'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1},
    'IMAGES_STORE': 'images/unsplash',
    'LOG_ENABLED': False,
    'COOKIES_ENABLED': False,
    'DOWNLOAD_DELAY': 2,
    'CLOSESPIDER_ITEMCOUNT': 10,
    'CONCURRENT_REQUESTS': 3,
    'MEDIA_ALLOW_REDIRECTS': True
  }

  if now > "11-15" or now < "01-15":
    # Christmas!! :D
    start_urls = ['https://unsplash.com/wallpapers/events/christmas','https://unsplash.com/images/events/christmas']
  else:
    start_urls = ['https://unsplash.com/wallpapers','https://unsplash.com/wallpapers/nature','https://unsplash.com/wallpapers/desktop']

  def parse(self, response):
    links = response.css('figure div a._2Mc8_::attr(href)').getall()
    images = response.css('div._13Q- > a._1yvXd::attr(href)').extract()

    for nextpage in links:
      nextpage = response.urljoin(nextpage)
      yield scrapy.Request(nextpage, self.parse)

    for item in images:
      yield {"image_urls":[item]}

# process = CrawlerProcess()
# process.crawl(UnsplashSpider)
# process.start()