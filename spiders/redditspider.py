import scrapy
# from scrapy.crawler import CrawlerProcess

class RedditSpider(scrapy.Spider):
  name = 'RedditSpider'
  custom_settings = {
    'ITEM_PIPELINES': {'scrapy.pipelines.images.ImagesPipeline': 1},
    'IMAGES_STORE': 'images/reddit',
    'LOG_ENABLED': False,
    'COOKIES_ENABLED': False,
    'DOWNLOAD_DELAY': 2,
    'CLOSESPIDER_ITEMCOUNT': 5
  }
  start_urls = ['https://www.reddit.com/r/wallpapers/top/']

  def parse(self, response):
    links = response.css('.SQnoC3ObvgnGjWt90zD9Z::attr(href)').getall()
    images = response.css('._3Oa0THmZ3f5iZXAQ0hBJ0k a::attr(href)').extract()
      
    for nextpage in links:
      nextpage = response.urljoin(nextpage)
      yield scrapy.Request(nextpage, self.parse)

    for item in images:
      yield {"image_urls":[item]}

# process = CrawlerProcess()
# process.crawl(RedditSpider)
# process.start()